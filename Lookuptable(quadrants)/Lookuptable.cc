// Look_up_table.c

#include <math.h>
#include "Lookuptable.h"
#include <iostream>

Lookuptable::Lookuptable() {		// Contrsuctor

	length = 1<<14;			// 2^16/4 values quarter of a sin wave
	quarterwtable = new double[length];		// Allocate memory for the array samples

	for(int i=0 ; i<length ; i++) {
		quarterwtable[i] = (sin(i*0.5*M_PI/length)); 	// Creates 1/4 sine samples and writes them to array quarterwtable	
	}
};

Lookuptable::~Lookuptable() {			// Destructor
	delete[] quarterwtable;			// Destroy samples
};
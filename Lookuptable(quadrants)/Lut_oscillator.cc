// Lookup Table Oscillator

#include <math.h>
#include "Lut_oscillator.h"
#include "buffer.h"
#include "Lookuptable.h"

#include <iostream>

Lut_oscillator::Lut_oscillator(){
};

void Lut_oscillator::generate(double a, double f, int sr, Buffer &b, Lookuptable &look) {
	
	amplitude = a;
	frequency = f;
	rate = sr;
	int size = b.getsize();			// returns the value of the private variable buffersize
	int length = 1<<16;			// 2^16 values
	int quadrant = 1<<14;

	unsigned int phaseinc = frequency*length*(length/static_cast<double>(rate));	// frequncy control
	unsigned int index = 0;				// Begin index at 0   |index (16bits)|fractional (16 bits)|

	for(int n=0; n<size; n++) {
		unsigned int shift_index = (index >> 16) & 0x3FFF;		// Shift unsigned int index along 16 bits to lose fractional part.
		
		switch(index>>30){
			case 0 :
				// do nothing
				b.mybuffer[n] = amplitude * look.quarterwtable[shift_index];	// Write values into mybuffer
				break;
			case 1 :
				// reflect
				b.mybuffer[n] = amplitude * look.quarterwtable[quadrant-shift_index];
				break;
			case 2 :
				// negate
				b.mybuffer[n] = -1 * amplitude * look.quarterwtable[shift_index];
				break;
			case 3 :
				// negate & reflect
				b.mybuffer[n] = -1 * amplitude * look.quarterwtable[quadrant-shift_index];
				break;
		}
		index += phaseinc;			// increment the index to go through sin wave at correct freqency
 	}
}
// LUT oscillator.h

#ifndef LUT_OSCILLATOR_H
#define LUT_OSCILLATOR_H

class Buffer;
class Lookuptable;

class Lut_oscillator {
private:
	double amplitude;
	double frequency;
	int rate;

public:
	Lut_oscillator();	// constructor
	void generate(double a, double f, int sr, Buffer &, Lookuptable &);	// generate function prototype, (pass buffer by reference)
};

#endif
// Buffer.cc

#include "buffer.h"

Buffer::Buffer(int size) {			// Constructor
	buffersize = size;
	mybuffer = new double[buffersize];	// Allocates memory
}

Buffer::~Buffer() {				// Destructor
	delete[] mybuffer;			// Frees up memory
}
// Main.cc

#include "Lut_oscillator.h"
#include "Lookuptable.h"
#include "buffer.h"
#include "sound_output_stream.h"

int main(int argc, char *argv[]) {
	
// Create buffer
Buffer b(100000);

// Create a Look up table called look by making an instance of the class
Lookuptable look;

// Call generate method from Lut_oscialltor class
Lut_oscillator osc;				// Makes an oscillator called osc
osc.generate(1.0, 440, 44100, b, look);		// Call the generate function


// Set up soundcard
Sound_output_stream sound(44100, 16, 2);
// Output values to soundcard
sound.sound_write(b);

}
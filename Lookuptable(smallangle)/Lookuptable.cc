// Look_up_table.c

#include <math.h>
#include "Lookuptable.h"
#include <iostream>


Lookuptable::Lookuptable() {		// Contrsuctor

	length = 10;			// 2^14 values - quarter of a sin wave
	quarterwtable = new double[length];		// Allocate memory for the array samples

	for(int i=0 ; i<length ; i++) {
		quarterwtable[i] = (sin(i*0.5*M_PI/(length-1))); 	// Creates 1/4 sine samples and writes them to array quarterwtable
	}
};

Lookuptable::~Lookuptable() {			// Destructor
	delete[] quarterwtable;			// Destroy samples
};
// Look_up_table.h

#ifndef LOOK_UP_TABLE_H
#define LOOK_UP_TABLE_H

class Lookuptable {

private:
	int length;

public:
	Lookuptable();		// Constructor
	~Lookuptable();		// Destructor
	//double *quarterwtable;
	double* quarterwtable;		// Define array for sin values
	int getlength(void) { return length; }
};

#endif
// Lookup Table Oscillator

#include <math.h>
#include "Lut_oscillator.h"
#include "buffer.h"
#include "Lookuptable.h"

#include <iostream>

Lookuptable Lut_oscillator::look;	// Create an instance of Lookuptable, as a property of oscillator, called look

Lut_oscillator::Lut_oscillator(){
};

void Lut_oscillator::generate(double a, double f, int sr, Buffer &b) {
	
	amplitude = a;
	frequency = f;			// PUT ALL OF THIS INTO LOOK UP TABLE...
	rate = sr;

	int tablelength = look.getlength() - 1;

	int size = b.getsize();			// returns the value of the private variable buffersize

	int length = 1<<16;			// length of full wave
	int quadrant = 1<<14;		// length of quadrant

	unsigned int phaseinc = frequency*length*(length/static_cast<double>(rate));	// frequncy control
	unsigned int index = 0;				// initialise index: |QUAD(2bits) actual index(14bits)|fractional (16 bits)|

	double delta = (M_PI/2) / static_cast<double>(tablelength); // distance between samples in radians
	

	for(int n=0; n<size; n++) {
		unsigned int shift_index = (index >> 16) & 0x3FFF;	// Shift index along to lose fractional part. Mask off two MSB
		double theta = (shift_index/static_cast<double>(quadrant)) * 0.5 * M_PI;	// point we want
		double sinA, cosA;					// define new variables for sin and cos parts
		unsigned int A = (theta/delta) + 0.5;	// a = closest sample rounded
		double B = theta - (A * delta);				//
		
		switch(index>>30){	// test which quadrant we're in.
			case 0 :
				// do nothing sin - reflect cos
				sinA = amplitude * look.quarterwtable[A];
				cosA = amplitude * look.quarterwtable[tablelength-A];
				break;
			case 1 :
				// reflect sin - negate cos
				sinA = amplitude * look.quarterwtable[tablelength-A];
				cosA = -1 * amplitude * look.quarterwtable[A];
				break;
			case 2 :
				// negate sin - negate & reflect cos
				sinA = -1 * amplitude * look.quarterwtable[A];
				cosA = -1 * amplitude * look.quarterwtable[tablelength-A];
				break;
			case 3 :
				// negate & reflect sin - do nothing to cos
				sinA = -1 * amplitude * look.quarterwtable[tablelength-A];
				cosA = amplitude * look.quarterwtable[A];
				break;
		}

		b.mybuffer[n] = sinA + B*cosA;	// copy values into the buffer
		index += phaseinc;		// increment the index by the frequency dependent phaseinc
 	}
}

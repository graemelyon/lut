// LUT oscillator.h

#ifndef LUT_OSCILLATOR_H
#define LUT_OSCILLATOR_H

class Buffer;
class Lookuptable;

class Lut_oscillator {
private:
	double amplitude;
	double frequency;
	int rate;
	static Lookuptable look;		// Make lookup table a static property.

public:
	Lut_oscillator();					// Constructor
	void generate(double a, double f, int sr, Buffer &);	// generate function prototype, (pass buffer by reference)
};

#endif
// Buffer.h

#ifndef BUFFER_H
#define BUFFER_H

class Buffer {

private:
	int buffersize;		// private property of buffer

public:
	Buffer(int);		// Constructor
	~Buffer();		// Destructor
	double *mybuffer;	// Creates array samples as a property of Buffer
	int getsize(){return buffersize;}	// buffersize is private so getsize() returns the size for public access as read only
};

#endif
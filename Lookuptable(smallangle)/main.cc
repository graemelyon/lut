// Main.cc

#include "Lut_oscillator.h"
#include "Lookuptable.h"
#include "buffer.h"
#include "sound_output_stream.h"

int main(int argc, char *argv[]) {
	
// Create buffer
Buffer b(88200);

// Call generate method from Lut_oscialltor class
Lut_oscillator osc;				// Makes an oscillator called osc
osc.generate(1.0, 220, 44100, b);		// Call the generate function

// Set up soundcard
Sound_output_stream sound(44100, 16, 2);
// Output values to soundcard
sound.sound_write(b);

}